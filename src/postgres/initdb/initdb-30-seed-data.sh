#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "feacna" <<-EOSQL
COPY uploaded_data.tnved1 FROM '/docker-entrypoint-initdb.d/TNVED1-015-20220113-0000-utf8.txt' DELIMITER '|';
COPY uploaded_data.tnved2 FROM '/docker-entrypoint-initdb.d/TNVED2-036-20220811-0000-utf8.txt' DELIMITER '|';
COPY uploaded_data.tnved3 FROM '/docker-entrypoint-initdb.d/TNVED3-020-20220811-0000-utf8.txt' DELIMITER '|';
COPY uploaded_data.tnved4 FROM '/docker-entrypoint-initdb.d/TNVED4-122-20220714-0000-utf8.txt' DELIMITER '|';
COPY uploaded_data.okpd2_section FROM '/docker-entrypoint-initdb.d/OKPD2-sections-01-35-utf8.txt' DELIMITER '|';
COPY uploaded_data.okpd2_origin FROM '/docker-entrypoint-initdb.d/OKPD2-01-35-utf8.txt' DELIMITER '|';
EOSQL
