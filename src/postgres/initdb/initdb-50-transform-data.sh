#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "feacna" <<-EOSQL

insert into tnved_section (id, name, comment, begin_date, end_date)
select id, name, comment, begin_date, end_date
from (select id, name, comment, begin_date, end_date, row_number() over(partition by id order by begin_date desc, end_date desc) as rn
        from (select razdel as id,
                     naim as name,
	                 trim(prim) as comment,
			         case when (trim(data) != '' and data is not null) then to_date(data, 'DD.MM.YYYY') end as begin_date,
			         case when (trim(data1) != '' and data1 is not null) then to_date(data1, 'DD.MM.YYYY') end as end_date
              from uploaded_data.tnved1) as t) as t1
where rn = 1;

insert into tnved_group (id, tnved_section_id, name, comment, begin_date, end_date)
select id, tnved_section_id, name, comment, begin_date, end_date
from (select id, tnved_section_id, name, comment, begin_date, end_date,
             row_number() over(partition by id order by begin_date desc, end_date desc) as rn
        from (select razdel as tnved_section_id,
		             gruppa as id,
                     naim as name,
	                 trim(prim) as comment,
			         case when (trim(data) != '' and data is not null) then to_date(data, 'DD.MM.YYYY') end as begin_date,
			         case when (trim(data1) != '' and data1 is not null) then to_date(data1, 'DD.MM.YYYY') end as end_date
              from uploaded_data.tnved2) as t) as t1
where rn = 1;

insert into tnved_position (id, tnved_group_id, position, name, begin_date, end_date)
select id, tnved_group_id, position, name, begin_date, end_date
from (select id, tnved_group_id, position, name, begin_date, end_date,
             row_number() over(partition by id order by begin_date desc, end_date desc) as rn
        from (select gruppa || tov_poz as id,
                     gruppa as tnved_group_id,
                     tov_poz as position,
                     naim as name,
			         case when (trim(data) != '' and data is not null) then to_date(data, 'DD.MM.YYYY') end as begin_date,
			         case when (trim(data1) != '' and data1 is not null) then to_date(data1, 'DD.MM.YYYY') end as end_date
              from uploaded_data.tnved3) as t) as t1
where rn = 1;

insert into tnved_subposition (id, tnved_position_id, tnved_group_id, position, subposition, short_name, begin_date, end_date)
select id, tnved_position_id, tnved_group_id, position, subposition, short_name, begin_date, end_date
from (select id, tnved_position_id, tnved_group_id, position, subposition, short_name, begin_date, end_date,
             row_number() over(partition by id order by begin_date desc, end_date desc) as rn
        from (select gruppa || tov_poz || sub_poz as id,
                     gruppa || tov_poz as tnved_position_id,
                     gruppa as tnved_group_id,
                     tov_poz as position,
                     sub_poz as subposition,
                     kr_naim as short_name,
			         case when (trim(data) != '' and data is not null) then to_date(data, 'DD.MM.YYYY') end as begin_date,
			         case when (trim(data1) != '' and data1 is not null) then to_date(data1, 'DD.MM.YYYY') end as end_date
              from uploaded_data.tnved4) as t) as t1
where rn = 1;

insert into okpd2 (id, okpd_class, okpd_subclass, okpd_group, okpd_subgroup, okpd_kind, okpd_category,
                   okpd_subcategory, name, description)
select id,
       substring(id from 1 for 2) as okpd_class,
       case when length(id) > 2 then substring(id from 1 for 4) end as okpd_subclass,
       case when length(id) > 4 then substring(id from 1 for 5) end as okpd_group,
       case when length(id) > 5 then substring(id from 1 for 7) end as okpd_subgroup,
       case when length(id) > 7 then substring(id from 1 for 8) end as okpd_kind,
       case when length(id) > 8 then substring(id from 1 for 11) || '0' end as okpd_category,
       case when (length(id) > 11 and substring(id from 12 for 12) <> '0') then id end as okpd_subcategory,
       name, description
from (
         select min(id) as id, min(name) as name, string_agg(description, E'\n') as description
         from (
                  select first_value(col1) over (partition by gr order by rowid) as id,
                         first_value(col2) over (partition by gr order by rowid) as name,
                         description,
                         gr
                  from (
                           select col1,
                                  col2,
                                  rowid,
                                  sum(start_of_group)
                                  over (order by rowid rows between unbounded preceding and current row) as gr,
                                  description
                           from (
                                    select col1,
                                           col2,
                                           CTID                                                        as rowid,
                                           case when trim(col1) = '' or col1 is null then 0 else 1 end as start_of_group,
                                           case when trim(col1) = '' or col1 is null then col2 end     as description
                                    from uploaded_data.okpd2_origin) as t
                       ) t1
              ) as t2
         group by gr
     ) as t3;

update okpd2 set parent_id = okpd_class where okpd_subclass is not null and okpd_group is null;
update okpd2 set parent_id = okpd_subclass where okpd_group is not null and okpd_subgroup is null;
update okpd2 set parent_id = okpd_group where okpd_subgroup is not null and okpd_kind is null;
update okpd2 set parent_id = okpd_subgroup where okpd_kind is not null and okpd_category is null;
update okpd2 set parent_id = okpd_kind where okpd_category is not null and okpd_subcategory is null;
update okpd2 set parent_id = okpd_category where okpd_subcategory is not null;

EOSQL