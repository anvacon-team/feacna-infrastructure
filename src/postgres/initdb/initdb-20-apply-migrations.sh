#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "feacna" <<-EOSQL

CREATE SCHEMA IF NOT EXISTS uploaded_data;

CREATE TABLE uploaded_data.tnved1 (
    razdel varchar(2) not null,
	naim varchar(4000),
	prim varchar(4000),
	data varchar(20) not null,
	data1 varchar(20)
);

CREATE TABLE uploaded_data.tnved2 (
    razdel varchar(2) not null,
	gruppa varchar(2) not null,
	naim varchar(4000),
	prim varchar(4000),
	data varchar(20) not null,
	data1 varchar(20)
);

CREATE TABLE uploaded_data.tnved3 (
	gruppa varchar(2) not null,
    tov_poz varchar(2) not null,
	naim varchar(4000),
	data varchar(20) not null,
	data1 varchar(20)
);

CREATE TABLE uploaded_data.tnved4 (
	gruppa varchar(2) not null,
    tov_poz varchar(2) not null,
	sub_poz varchar(6) not null,
	kr_naim varchar(200),
	data varchar(20) not null,
	data1 varchar(20)
);

CREATE TABLE uploaded_data.okpd2_section (
    id varchar(1) not null,
	title varchar(10) not null,
	name varchar(100) not null,
	begin_code varchar(10) not null,
	end_code varchar(10) not null
);

CREATE TABLE uploaded_data.okpd2_origin (
    col1 varchar(2000),
	col2 varchar(2000)
);

CREATE TABLE tnved_section (
    id varchar(2) not null,
	name varchar(4000),
	comment varchar(4000),
	begin_date date not null,
	end_date date,
    CONSTRAINT pk_tnved_section PRIMARY KEY (id)
);

CREATE TABLE tnved_group (
    id varchar(2) not null,
    tnved_section_id varchar(2) not null,
	name varchar(4000),
	comment varchar(4000),
	begin_date date not null,
	end_date date,
    CONSTRAINT pk_tnved_group PRIMARY KEY (id)
);

CREATE TABLE tnved_position (
    id varchar(4) not null,
    tnved_group_id varchar(2) not null,
    position varchar(2) not null,
	name varchar(4000),
	begin_date date not null,
	end_date date,
    CONSTRAINT pk_tnved_position PRIMARY KEY (id)
);

CREATE TABLE tnved_subposition (
    id varchar(10) not null,
    tnved_position_id varchar(4) not null,
    tnved_group_id varchar(2) not null,
    position varchar(2) not null,
	subposition varchar(6) not null,
	short_name varchar(200),
	begin_date date not null,
	end_date date,
    CONSTRAINT pk_tnved_subposition PRIMARY KEY (id)
);

CREATE TABLE okpd2 (
    id varchar(12) not null,
	parent_id varchar(12) null,
	okpd_class varchar(2) not null,
	okpd_subclass varchar(4),
	okpd_group varchar(5),
	okpd_subgroup varchar(7),
	okpd_kind varchar(8),
	okpd_category varchar(12),
	okpd_subcategory varchar(12),
	name varchar(2000) not null,
	description varchar(2000),
    CONSTRAINT pk_okpd2 PRIMARY KEY (id)
);

EOSQL
